package com.epam.training.concurrency.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class PassengerTest {

    MovementDirection movementDirection;
    Passenger passenger;

    @Test
    public void getMovementDirection() {
        passenger = new Passenger(2, 3, 2);
        movementDirection = passenger.getMovementDirection();
        assertEquals(MovementDirection.DOWN, movementDirection);
        assertNotNull(movementDirection);
    }
}
