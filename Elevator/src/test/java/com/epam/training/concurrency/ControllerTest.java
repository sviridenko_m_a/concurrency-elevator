package com.epam.training.concurrency;

import com.epam.training.concurrency.model.Building;
import com.epam.training.concurrency.model.Elevator;
import com.epam.training.concurrency.model.Floor;
import com.epam.training.concurrency.model.Passenger;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ControllerTest {

    private Elevator elevator = new Elevator(5, 2);
    private Building building = new Building(asList(new Floor(), new Floor(), new Floor()), elevator);
    private Controller controller = new Controller(building);
    private Passenger passenger;
    private Floor floor = new Floor();

    @Test
    public void moveElevator() {
        elevator = new Elevator(5, 2);
        controller.moveElevator(elevator);
        assertEquals(2, elevator.getCurrentFloor());
    }

    @Test
    public void moveElevatorDown() {
        elevator = new Elevator(5, 2);
        elevator.setCurrentFloor(5);
        controller.moveElevator(elevator);
        assertEquals(4, elevator.getCurrentFloor());
    }

    @Test
    public void boardingPassenger() {
        floor = building.getFloors().get(2);
        elevator = new Elevator(5, 2);
        elevator.setCurrentFloor(2);
        int sizeElevator = elevator.getContainer().size();
        int sizeDispatch = floor.getDispatchContainer().size();
        passenger = new Passenger(2, 2, 3);
        controller.boardingPassenger(passenger, building);
        int sizeElevatorAfterBoarding = elevator.getContainer().size();
        int sizeDispatchAfterBoarding = floor.getDispatchContainer().size();
        assertEquals(sizeElevator, sizeElevatorAfterBoarding);
        assertEquals(sizeDispatch, sizeDispatchAfterBoarding);

    }

    @Test
    public void landingPassenger() {
        floor = building.getFloors().get(2);
        elevator = new Elevator(5, 2);
        elevator.setCurrentFloor(2);
        int sizeElevator = elevator.getContainer().size();
        int sizeDispatch = floor.getDispatchContainer().size();
        passenger = new Passenger(2, 2, 3);
        controller.landingPassenger(passenger, building);
        int sizeElevatorAfterBoarding = elevator.getContainer().size();
        int sizeDispatchAfterBoarding = floor.getDispatchContainer().size();
        assertEquals(sizeElevator, sizeElevatorAfterBoarding);
        assertEquals(sizeDispatch, sizeDispatchAfterBoarding);
    }

    @Test
    public void isAnybodyNotInArrivalContainer() {
        passenger = mock(Passenger.class);
        floor.getArrivalContainer().add(passenger);
        elevator.getContainer().add(passenger);
        assertTrue(controller.isAnybodyNotInArrivalContainer());
    }

    @Test
    public void isAllPersonsInArrivalContainer() {
        passenger = mock(Passenger.class);
        floor.getArrivalContainer().add(passenger);
        assertFalse(controller.isAllPersonsInArrivalContainer());
    }
}