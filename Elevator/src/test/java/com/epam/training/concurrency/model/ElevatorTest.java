package com.epam.training.concurrency.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class ElevatorTest {

    Elevator elevator;
    Passenger passenger;
    Floor floor;

    @Test
    public void getNumFreePlaces() {
        elevator = new Elevator(5, 2);
        assertEquals(2, elevator.getNumFreePlaces());
        assertNotNull(elevator.getNumFreePlaces());
    }
}