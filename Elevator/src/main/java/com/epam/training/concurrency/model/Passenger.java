package com.epam.training.concurrency.model;

public class Passenger {
	 
	private final int passengerId;
	private final int soursFloor;
	private final int destinationFloor;

	private TransportationState transportationState;

	public Passenger(int passengerId, int soursFloor,  int destinationFloor) {
		super();
		this.passengerId = passengerId;
		this.soursFloor = soursFloor;
		this.destinationFloor = destinationFloor;
	}

	public TransportationState getTransportationState() {
		return transportationState;
	}

	public void setTransportationState(TransportationState transportationState) {
		this.transportationState = transportationState;
	}

	public int getPassengerId() {
		return passengerId;
	}

	public int getSoursFloor() {
		return soursFloor;
	}

	public int getDestinationFloor() {
		return destinationFloor;
	}

	/**
	 * Method return passengers movement direction
	 * @return - movement direction
	 */
	public MovementDirection getMovementDirection() {
		if(getSoursFloor() < getDestinationFloor()) {
			return MovementDirection.UP;
		} else {
			return MovementDirection.DOWN;
		}
	}
}