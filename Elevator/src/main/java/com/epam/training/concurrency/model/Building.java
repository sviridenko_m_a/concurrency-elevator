package com.epam.training.concurrency.model;

import java.util.List;
 
public class Building {
	
	private final List<Floor> floors;
	private final Elevator elevator;

	public Building(List<Floor> floors, Elevator elevator) {
		super();
		this.floors = floors;
		this.elevator = elevator;
	}

	public List<Floor> getFloors() {
		return floors;
	}

	public Elevator getElevator() {
		return elevator;
	}
}
