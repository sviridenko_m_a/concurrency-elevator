package com.epam.training.concurrency.model;

import java.util.ArrayList;
import java.util.List;

public class Elevator {

    private final static int START_FLOOR = 1;
    private final int topFloor;
    private final int capacity;
    private final List<Passenger> container;

    private int currentFloor;
    private MovementDirection movementDirection;

    public Elevator(int topFloor, int capacity) {
        super();
        this.topFloor = topFloor;
        this.container = new ArrayList<>(capacity);
        this.capacity = capacity;
        this.currentFloor = START_FLOOR;
        this.movementDirection = MovementDirection.UP;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public MovementDirection getMovementDirection() {
        return movementDirection;
    }

    public void setMovementDirection(MovementDirection movementDirection) {
        this.movementDirection = movementDirection;
    }

    public List<Passenger> getContainer() {
        return container;
    }

    public int getTopFloor() {
        return topFloor;
    }

    private int getCapacity() {
        return capacity;
    }

    /**
     * Method return number elevator free places
     */
    public int getNumFreePlaces() {
        return getCapacity() - getContainer().size();
    }

}