package com.epam.training.concurrency;

import com.epam.training.concurrency.configs.ConfigReader;
import com.epam.training.concurrency.model.*;
import org.apache.log4j.Logger;

public class Controller {

private static Logger APP_FILE = Logger.getLogger("APP_FILE");
    private final Building building;

    public Controller(Building building) {
        this.building = building;
    }

    /**
     * Method moves elevator up or down
     *
     * @param elevator - elevator
     */
    public void moveElevator(Elevator elevator) {
        int currentFloor = elevator.getCurrentFloor();
        MovementDirection movementDirection = elevator.getMovementDirection();
        if (MovementDirection.UP.equals(movementDirection)) {
            if (currentFloor == elevator.getTopFloor()) {
                elevator.setMovementDirection(MovementDirection.DOWN);
                elevator.setCurrentFloor(currentFloor - 1);
            } else {
                elevator.setCurrentFloor(currentFloor + 1);
            }
            APP_FILE.info("MOVING_ELEVATOR_FROM_THE_FLOOR " + currentFloor + " TO " + elevator.getCurrentFloor());
        } else {
            if (currentFloor == 1) {
                elevator.setMovementDirection(MovementDirection.UP);
                elevator.setCurrentFloor(currentFloor + 1);
            } else {
                elevator.setCurrentFloor(currentFloor - 1);
            }
            APP_FILE.info("MOVING_ELEVATOR_FROM_THE_FLOOR " + currentFloor + " TO " + elevator.getCurrentFloor());
        }
    }

    /**
     * Method remove passenger from the dispatch container and add to the elevator container.
     *
     * @param passenger - passenger
     * @param building  - building
     */
    public synchronized void boardingPassenger(Passenger passenger, Building building) {
        building.getElevator().getContainer().add(passenger);
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
        building.getFloors().get(passenger.getSoursFloor()).getDispatchContainer().remove(passenger);
        notifyAll();
        APP_FILE.info("BOARDING_PASSENGER FROM THE FLOOR №" + (passenger.getSoursFloor() + 1)
                + " TO THE FLOOR №" + (passenger.getDestinationFloor() + 1)
                + " (passengerID: " + passenger.getPassengerId() + ")");
    }

    /**
     * Method remove passenger from the elevator container and add to the arrival container.
     *
     * @param passenger - passenger
     * @param building  - building
     */
    public synchronized void landingPassenger(Passenger passenger, Building building) {
        passenger.setTransportationState(TransportationState.COMPLETED);
        building.getFloors().get(building.getElevator().getCurrentFloor() - 1).getArrivalContainer().add(passenger);
        building.getElevator().getContainer().remove(passenger);
        APP_FILE.info("LANDING_PASSENGER TO THE FLOOR №" + (passenger.getDestinationFloor() + 1)
                + "(passengerID: " + passenger.getPassengerId() + ")");
        notifyAll();
    }

    /**
     * Method check and return true if any body stay at the dispatch container or elevator container
     * Otherwise return false.
     */
    public boolean isAnybodyNotInArrivalContainer() {
        for (Floor floor : building.getFloors()) {
            if (!floor.getDispatchContainer().isEmpty() || !building.getElevator().getContainer().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method check and return true if all passengers are at the arrival container
     * Otherwise return false.
     */
    public boolean isAllPersonsInArrivalContainer() {
        int sum = 0;
        for (Floor floor : building.getFloors()) {
            sum = sum + floor.getArrivalContainer().size();
        }
        return sum == ConfigReader.getInstance().getPassengersNumber();
    }
}
