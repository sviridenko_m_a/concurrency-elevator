package com.epam.training.concurrency.task;

import com.epam.training.concurrency.Controller;
import com.epam.training.concurrency.model.*;

public class PassengerTransportationTask extends Thread {

    private final Passenger passenger;
    private final Controller controller;
    private final Building building;

    public PassengerTransportationTask(Passenger passenger, Controller controller, Building building) {
        this.passenger = passenger;
        this.controller = controller;
        this.building = building;
    }

    public void run() {
        while (passenger.getTransportationState() != TransportationState.COMPLETED) {

            synchronized (building.getElevator().getContainer()) {
                while (passenger.getTransportationState() == TransportationState.IN_PROGRESS
                        && (building.getElevator().getCurrentFloor() - 1) == passenger.getDestinationFloor()) {
                    controller.landingPassenger(passenger, building);
                }
            }

            synchronized (building.getFloors().get(building.getElevator().getCurrentFloor() - 1).getDispatchContainer()) {
                while (passenger.getTransportationState() == TransportationState.NOT_STARTED
                        && passenger.getSoursFloor() == (building.getElevator().getCurrentFloor() - 1)
                        && passenger.getMovementDirection() == (building.getElevator().getMovementDirection())
                        && building.getElevator().getNumFreePlaces() != 0) {
                    controller.boardingPassenger(passenger, building);
                }
            }

        }
    }
}