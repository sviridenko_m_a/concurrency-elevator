package com.epam.training.concurrency.task;

import com.epam.training.concurrency.Controller;
import com.epam.training.concurrency.model.*;

public class ElevatorMovementTask implements Runnable {

    private final Controller controller;
    private final Building building;
    private final Elevator elevator;

    public ElevatorMovementTask(Controller controller, Building building, Elevator elevator) {
        this.controller = controller;
        this.building = building;
        this.elevator = elevator;
    }

    public void run() {
        synchronized (building.getFloors()) {
            while (controller.isAnybodyNotInArrivalContainer() & !controller.isAllPersonsInArrivalContainer()) {
                controller.moveElevator(elevator);
            }
        }
    }
}
