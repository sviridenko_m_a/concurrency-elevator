package com.epam.training.concurrency.utils;

import com.epam.training.concurrency.model.Building;
import com.epam.training.concurrency.model.Elevator;
import com.epam.training.concurrency.model.Floor;

public class Validation {

    public boolean isDispatchContainerEmpty(Building building){
        for (Floor floor : building.getFloors())
           return floor.getDispatchContainer().isEmpty();
        return false;
    }

    public boolean isElevatorContainerEmpty(Elevator elevator){
        return elevator.getContainer().isEmpty();
    }

    public int PassengersNumberInArrivalContainer(Building building){
        int passengersSumm = 0;
        for (Floor floor : building.getFloors()){
            passengersSumm = passengersSumm + floor.getArrivalContainer().size();
        }
        return passengersSumm;
    }
}
