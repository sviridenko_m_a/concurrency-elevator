package com.epam.training.concurrency.configs;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigReader {

    private final static Logger LOGGER = Logger.getLogger(ConfigReader.class.getName());
    private final static String FILE_NAME = "config.properties";
    private final static String ELEVATOR_CAPACITY = "elevatorCapacity";
    private final static String PASSENGERS_NUMBER = "passengersNumber";
    private final static String FLOORS_NUMBER = "floorsNumber";

    private int floorsNumber;
    private int elevatorCapacity;
    private int passengersNumber;

    private static ConfigReader instance = new ConfigReader(FILE_NAME);

    /**
     * Class constructor reed file .properties to get number of elevator passengers,
     * total passengers number and floors number in the building
     *
     * @param fileName - name of .properties file
     */
    private ConfigReader(String fileName) {
        Properties property = new Properties();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        try {
            if (inputStream != null) {
                property.load(inputStream);
            }
        } catch (IOException e) {
            LOGGER.error("Config file was broken or not found, see 'config/ConfigReader'", e);
        }
        floorsNumber = Integer.parseInt(property.getProperty(FLOORS_NUMBER));
        elevatorCapacity = Integer.parseInt(property.getProperty(ELEVATOR_CAPACITY));
        passengersNumber = Integer.parseInt(property.getProperty(PASSENGERS_NUMBER));
    }

    public static ConfigReader getInstance() {
        return instance;
    }

    public int getFloorsNumber() {
        return floorsNumber;
    }

    public int getElevatorCapacity() {
        return elevatorCapacity;
    }

    public int getPassengersNumber() {
        return passengersNumber;
    }
}
