package com.epam.training.concurrency.model;

import java.util.ArrayList;
import java.util.List;

public class Floor {

    private List<Passenger> dispatchContainer;
    private List<Passenger> arrivalContainer;

    public Floor() {
        super();
        this.dispatchContainer = new ArrayList<>();
        this.arrivalContainer = new ArrayList<>();
    }

    public List<Passenger> getDispatchContainer() {
        return dispatchContainer;
    }

    public List<Passenger> getArrivalContainer() {
        return arrivalContainer;
    }
}