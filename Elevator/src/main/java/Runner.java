import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.epam.training.concurrency.Controller;
import com.epam.training.concurrency.task.PassengerTransportationTask;
import com.epam.training.concurrency.utils.Validation;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;

import com.epam.training.concurrency.model.*;
import com.epam.training.concurrency.configs.ConfigReader;
import com.epam.training.concurrency.task.ElevatorMovementTask;

public class Runner {

    private static Logger APP_FILE = Logger.getLogger("APP_FILE");
    private static Logger LOGGER_CONSOLE = Logger.getLogger("APP_CONSOLE");

    public static void main(String[] args) {
        ConfigReader reader = ConfigReader.getInstance();
        int floorsNumber = reader.getFloorsNumber();
        int elevatorCapacity = reader.getElevatorCapacity();
        int passengersNumber = reader.getPassengersNumber();

        List<Floor> floors = new ArrayList<>();
        for (int i = 0; i < floorsNumber; i++) {
            floors.add(new Floor());
        }

        Elevator elevator = new Elevator(floorsNumber, elevatorCapacity);
        Building building = new Building(floors, elevator);
        Controller controller = new Controller(building);

        int numDispatchFloor;
        int numDestinationFloor;

        Random random = new Random();
        Passenger passenger;
        ExecutorService executor;
        APP_FILE.info("START_TRANSPORTATION");
        LOGGER_CONSOLE.info("START_TRANSPORTATION");

        for (int i = 0; i < passengersNumber; i++) {
            numDispatchFloor = Math.abs(random.nextInt(floorsNumber));
            numDestinationFloor = Math.abs(random.nextInt(floorsNumber));
            if (numDestinationFloor == numDispatchFloor & numDestinationFloor != 0
                    & numDestinationFloor != 1 || numDestinationFloor > floorsNumber) {
                numDestinationFloor--;
            }
            if (numDestinationFloor == numDispatchFloor & (numDestinationFloor == 0 || numDestinationFloor == 1)) {
                numDestinationFloor++;
            }

            passenger = new Passenger(i, numDispatchFloor, numDestinationFloor);
            passenger.setTransportationState(TransportationState.NOT_STARTED);
            floors.get(numDispatchFloor).getDispatchContainer().add(passenger);
            executor = Executors.newFixedThreadPool(passengersNumber);

            executor.submit(new PassengerTransportationTask(passenger, controller, building));
        }
        executor = Executors.newFixedThreadPool(1);
        executor.submit(new ElevatorMovementTask(controller, building, elevator));
        executor.shutdown();

        while (!executor.isTerminated());
        APP_FILE.info("COMPLETION_TRANSPORTATION");
        LOGGER_CONSOLE.info("COMPLETION_TRANSPORTATION");


        Validation validation = new Validation();
        APP_FILE.info("Passengers sum in arrival container: "+ validation.PassengersNumberInArrivalContainer(building));
        APP_FILE.info("Is all passengers landing from dispatch container? result: "+ validation.isDispatchContainerEmpty(building));
        APP_FILE.info("Is all passengers landing from elevator container? result: "+ validation.isElevatorContainerEmpty(elevator));
        LOGGER_CONSOLE.info("Passengers sum in arrival container: "+ validation.PassengersNumberInArrivalContainer(building));
        LOGGER_CONSOLE.info("Is all passengers landing from dispatch container? result: "+ validation.isDispatchContainerEmpty(building));
        LOGGER_CONSOLE.info("Is all passengers landing from elevator container? result: "+ validation.isElevatorContainerEmpty(elevator));
    }
}